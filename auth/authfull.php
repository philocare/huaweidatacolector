<?php


define('SAMPLESET', 'sampleSet');
define('DATACOLLECTORID', 'dataCollectorId');
define('SAMPLEPOINTS', 'samplePoints');
define('STARTTIME', 'startTime');
define('ENDTIME', 'endTime');
define('VALUE', 'value');
define('FIELDNAME', 'fieldName');
define('FLOATVALUETYPE', 'floatValue');
define('INTEGERVALUETYPE', 'integerValue');
define('INDEX0', 0);
define('STEPSTYPE', 'com.huawei.continuous.steps.delta');
define('HRTYPE', 'com.huawei.instantaneous.heart_rate');
define('SLEEPTYPE', 'com.huawei.continuous.sleep.fragment');
define('VALUETYPEHR', 'bpm');
define('CONVERTTOSECONDS', 1000000000);

// O token que permite o acesso ao usuário
// Cada usuário terá de ter seu token armazenado para podermos ter acesso ao seus dados.
// Um link de autorização deve ser enviado ao usuário para que ele autorize a Philocare utilizar seus dados
//
// Colocar as autorizações philocare
$steps_samples = array();
$hr_samples = array();


function ProcessSamples($sampleset)
{

    //dump($sampleset['group']);
    //echo '<br>';
    //echo "## Número de amostras: ";
    //echo sizeof($sampleset['group']) . '<br>';
    //echo "\r\n<br>";
    $sample_point_startime = null;
    $sample_point_endtime = null;

    $data_stream_steps = '';
    $data_stream_hr = '';
    $data_stream_sleep = '';

    $data_stream = array();

    // Grupos de dados
    foreach ($sampleset as $key => $group_index) {
        // Amostras do grupo
        foreach ($group_index as $key1 => $index_sample_set) {
            $stepSampleSet = $index_sample_set; //$sampleset['group'][0];
            //echo "## Propriedade das amostras: ";
            //echo sizeof($stepSampleSet) . '<br>';

            echo 'Hora incial da coleta: ' . $stepSampleSet['startTime'] . '<br>';
            echo 'Hora final da coleta: ' . $stepSampleSet['endTime'] . '<br>';

            echo "## Tipo de amostras: ";
            echo $stepSampleSet['sampleSet'][0]['dataCollectorId'] . '<br>';

            $datatype = $stepSampleSet[SAMPLESET][0][DATACOLLECTORID];
            // Verifica se são passos
            if (strpos($datatype, STEPSTYPE)) {
                $data_stream_steps = ($stepSampleSet['startTime']/1000) . ',' .
                    ($stepSampleSet['endTime']/1000) . ',';
            }
            if (strpos($datatype, HRTYPE)) {
                $data_stream_hr = ($stepSampleSet['startTime']/1000) . ',' .
                    ($stepSampleSet['endTime']/1000) . ',';
            }
            if (strpos($datatype, SLEEPTYPE)) {
                $data_stream_sleep = ($stepSampleSet['startTime']/1000) . ',' .
                    ($stepSampleSet['endTime']/1000) . ',';
            }

            echo "## Número de amostras do tipo: " . $datatype . "<br>";
            echo sizeof($stepSampleSet['sampleSet'][0]['samplePoints']) . '<br>';

            // todas os valores
            $fullpoints = $stepSampleSet[SAMPLESET][INDEX0][SAMPLEPOINTS];

            if (sizeof($fullpoints) == 0) {
                // Verifica se são passos
                if (strpos($datatype, STEPSTYPE)) {
                    $data_stream_steps = "";
                    continue;
                }
                if (strpos($datatype, HRTYPE)) {
                    $data_stream_hr = "";
                    continue;
                }
                if (strpos($datatype, SLEEPTYPE)) {
                    $data_stream_sleep = "";
                    continue;
                }

            }
            // loop para pegar todos timestemps e valores
            foreach ($fullpoints as $key2 => $point_prop) {
                //echo "Hora inicial: " . $point_prop["startTime"] . "<br>";
                //echo "Hora final: " . $point_prop["endTime"] . "<br>";
                //echo "Tipo de dado: " . $point_prop["value"][0]["fieldName"] . "<br>";

                $pointStartTime = $point_prop[STARTTIME] / CONVERTTOSECONDS;
                $pointEndTime = $point_prop[ENDTIME] / CONVERTTOSECONDS;
                $type = $point_prop[VALUE][INDEX0][FIELDNAME];
                if ($type == VALUETYPEHR) {
                    //echo "Valor: " . $point_prop["value"][0]["floatValue"] . "<br>";
                    $pointValue = $point_prop[VALUE][INDEX0][FLOATVALUETYPE];
                } else {
                    //echo "Valor: " . $point_prop["value"][0]["integerValue"] . "<br>";
                    $pointValue = $point_prop[VALUE][INDEX0][INTEGERVALUETYPE];
                }
                // Verifica se são passos
                if (strpos($datatype, STEPSTYPE)) {
                    $data_stream_steps = $data_stream_steps . $pointStartTime . ',' . $pointEndTime . ',' . $pointValue . ',';
                }
                if (strpos($datatype, HRTYPE)) {
                    $data_stream_hr = $data_stream_hr . $pointStartTime . ',' . $pointEndTime . ',' . $pointValue . ',';
                }
                if (strpos($datatype, SLEEPTYPE)) {
                    $data_stream_sleep = $data_stream_sleep . $pointStartTime . ',' . $pointEndTime . ',' . $pointValue . ',';
                }
            }
        }
    }

    //dump($stepSampleSet["sampleSet"][0]['samplePoints']);
    // Eliminar o último ponto e virgula de cada um
    $data_stream_steps = substr($data_stream_steps, 0, -1);
    $data_stream_hr = substr($data_stream_hr, 0, -1);
    $data_stream_sleep = substr($data_stream_sleep, 0, -1);


    array_push($data_stream, $data_stream_steps);
    array_push($data_stream, $data_stream_hr);
    array_push($data_stream, $data_stream_sleep);

    return $data_stream;
}


/**
 * Função para processar as amostras retornadas pelo Healthkit
 *
 * @param $sampleset - sampletset of JSON
 */
//function ProcessSamples($sampleset) {
//
//    //    echo "## Número de amostras: ";
//    //echo sizeof($sampleset);
//    //echo "\r\n<br>";
//    $sample_point_startime = null;
//    $sample_point_endtime = null;
//
//    foreach($sampleset as $key => $sample_index) {
//        foreach($sample_index as $key1 => $index_sample_point) {
//            foreach($index_sample_point as $key2 => $sample_point) {
//                foreach ($sample_point as $key3 => $value) {
//                    echo $key . " => " . $key1 . " => " . $key2 . " => " . $key3 . " => " . $value . "<br>";
//
//                    if ($key3 == 'startTime') {
//                        $sample_point_startime = $value/1000000000;
////                        $startDateTime = new DateTime();
////                        $startDateTime->setTimestamp($sample_point_startime);
////                        $startDateTime->setTimezone(new DateTimeZone('Brazil/East'));
//                    }
//                    if ($key3 == 'endTime') $sample_point_endtime = $value;
//                    if ($key3 == 'dataTypeName') {
//                        // verifica qual tipo de informação
//                        // passo?
//                        if ($value == 'com.huawei.continuous.steps.delta') {
//                            $point_value = $sampleset[$key][$key1][$key2]['value'];
//                            foreach ($point_value as $point_index => $point_valued) {
//                                foreach ($point_valued as $prop => $prop_value) {
//                                    //echo $point_index . " == " . $prop . " == " . $prop_value;
//                                    if ($prop == "integerValue") {
//                                        $startDateTime = new DateTime();
//                                        $startDateTime->setTimestamp($sample_point_startime);
//                                        $startDateTime->setTimezone(new DateTimeZone('Brazil/East'));
//                                        echo "Passos: " .  $startDateTime->format('d/m/Y H:i:s') . " = " . $prop_value . "<br>";
//                                    }
//                                }
//                            }
//                        }
//                        // bpm?
//                        if ($value == 'com.huawei.instantaneous.heart_rate') {
//                            $point_value = $sampleset[$key][$key1][$key2]['value'];
//                            foreach ($point_value as $point_index => $point_valued) {
//                                foreach ($point_valued as $prop => $prop_value) {
//                                    //echo $point_index . " == " . $prop . " == " . $prop_value;
//                                    if ($prop == "floatValue") {
//                                        $startDateTime = new DateTime();
//                                        $startDateTime->setTimestamp($sample_point_startime);
//                                        $startDateTime->setTimezone(new DateTimeZone('Brazil/East'));
//                                        echo "HR: " .  $startDateTime->format('d/m/Y H:i:s') . " = " . $prop_value . "<br>";
//                                    }
//                                }
//                            }
//                        }
//                        // sleep?
//                        if ($value == 'com.huawei.continuous.sleep.fragment') {
//                            $point_value = $sampleset[$key][$key1][$key2]['value'];
//                            foreach ($point_value as $point_index => $point_valued) {
//                                foreach ($point_valued as $prop => $prop_value) {
//                                    //echo $point_index . " == " . $prop . " == " . $prop_value;
//                                    if ($prop == "integerValue") {
//                                        $startDateTime = new DateTime();
//                                        $startDateTime->setTimestamp($sample_point_startime);
//                                        $startDateTime->setTimezone(new DateTimeZone('Brazil/East'));
//                                        echo "Sleep: " .  $startDateTime->format('d/m/Y H:i:s') . " = " . $prop_value . "<br>";
//                                    }
//                                }
//                            }
//                        }                    }
//                }
//            }
//        }
//    }
////
////    echo "\r\n<br>";
////
////    foreach($resp['group'][0]['sampleSet'][0]['samplePoints'][0] as $key => $value) {
////        foreach($value as $key1 => $value1) {
////            foreach($value1 as $key2 => $value2) {
////                echo $key . " => " . $key1 . " => " . $key2 . " => " . $value2 . "<br>";
////            }
////        }
////    }
//
//}

if (!isset($_GET["code"])) {

    //echo "<h1>Início</h1><br><br>";

    // 1. Generate an authorization code
    // url de requisição do código oAuth
    $oauth_url = "https://oauth-login.cloud.huawei.com/oauth2/v3/authorize?";

    // resposta
    $resp_type = "response_type=code";
    // status
    $stt_auth = "&state=state_parameter_passthrough_value";
    // Código Philo
    $client_code = "&client_id=103879243";
    // Url de redirecionamento
    $redir_url = "&redirect_uri=https://rest.philo.solutions/auth/authfull.php";
    // Escopo da leitura
    //$scope_detalis = "&scope=openid+https://www.huawei.com/healthkit/step.read";
    $scope_detalis = "&scope=openid+https://www.huawei.com/healthkit/step.read+https://www.huawei.com/healthkit/sleep.read+https://www.huawei.com/healthkit/heartrate.read";

    //+https://www.huawei.com/healthkit/step.read+https://www.huawei.com/healthkit/sleep.read";
    // tipo de acesso
    $access_type = "&access_type=offline";
    // display
    $disp = "&display=touch&t&";

    // constroi o pedido de get
    $get_str = $oauth_url . $resp_type . $stt_auth . $client_code . $redir_url . $scope_detalis . $access_type . $disp;

    header("Location: " . $get_str);

// Monta o GET
//$ch = curl_init($get_str);

//$return = curl_exec($ch);

//curl_close($ch);

//return $return;
} else {
    // Atribuição de parâmetros
    //$null = "";
    //$dataPath = "D:/php/Apache2.4.41/Apache24/htdocs/json.txt";
    //$dataCollectorId = "raw:com.huawei.instantaneous.height:com.huawei.demo:huawei:ExampleTablet:1000:DataCollectorExample";
    //$url = "https://hostname/healthkit/v1/dataCollectors";

    $accessCode = $_GET["code"];

    //    echo "Código de acesso obtido.<br>";
    //    echo "Requisitando TOKEN<br>";

    $url="https://oauth-login.cloud.huawei.com/oauth2/v3/token"; //Request address

    $param = array(
        //set value as "authorization_code"；meaning Use Authorization Code to get Access Token and ID Token.
        "grant_type" => "authorization_code",
        //Appid of the application registered on the developer Alliance.
        "client_id" => "103879243",
        //Public key assigned to the application which created in developer alliance,please refrencing the interface description for using.
        "client_secret" => "fb29d356175d3912d33e4bd3316424f504b247b5b367cf2da570cc9e26c9e5ea",
        //Random number which used for verify the code,please refrencing the interface description for using.
        //"code_verifier" => "123444444dfd4sadfsdwew321454567587658776t896fdfgdscvvbfxdgfdgfdsfasdfsdgd233",
        //Callback url of application configuration.
        "redirect_uri" => "https://rest.philo.solutions/auth/authfull.php",
        "code"=>$accessCode
    );


    $ch = curl_init();
    $header[] = "Content-Type: application/x-www-form-urlencoded";
    $content = http_build_query($param, "", "&");


    $header[] = "Content-Length: ".strlen($content);
    curl_setopt($ch, CURLOPT_HEADER, true); //setting output include header.
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header); //setting the transferred content in the header.
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, count($param));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // check the source of the certificate or not.
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE); // check the source of the certificate or not.
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // setting not output all content if faild automatically
    $response = curl_exec($ch);
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $result = substr($response, $header_size);
    curl_close($ch);

    $json_res = json_decode($result, JSON_PRETTY_PRINT);

    //    echo "Resposta a requisição de TOKEN acesso: <br>";
    //    echo "Tempo token: " . $json_res['expires_in'] . "<br>";
    //    echo "Escopo: " . $json_res['scope'] . "<br>";
    //    echo 'Access token: '. $json_res['access_token']. '<br>';
    //    echo "Id token: " . $json_res['id_token'] . '<br><br>';

    $accessToken = $json_res['access_token'];
    $idToken = $json_res['id_token'];

    $url = "https://health-api.cloud.huawei.com/healthkit/v1/sampleSet:polymerize";
    $ch = curl_init();//Inicializar curl
    $timeout = 10;// Tempo limite (unidade: s)
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);//O resultado é uma string e a saída para a tela
    curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);//Definir tempo limite
    curl_setopt ($ch, CURLOPT_CUSTOMREQUEST, "POST");//Solicitar método de envio
    curl_setopt ($ch, CURLOPT_URL, $url);// Solicitar endereço de url

    $postdata = file_get_contents("pjson.txt");

    curl_setopt($ch, CURLOPT_POSTFIELDS,$postdata);

    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Bearer '. $accessToken
            //'Content-Length: '.strlen($postdata)
        )
    );

    $file_contents = curl_exec($ch);//Execute curl e obtenha o valor de retorno
    curl_close($ch);

    //echo "Resposta polymerize: ";
    $resp = json_decode($file_contents, JSON_PRETTY_PRINT);

    //##################################################################################################################
    // Decodificar a resposta
    //echo "### Grupo:  ";
    //print_r($resp['group']);
    //echo "\r\n<br>";
    echo "## Grupos de dados: ";
    echo sizeof($resp['group']);
    echo "\r\n<br>";


    ProcessSamples($resp);


    // Número de grupos de dados retornado
    $num_groups = sizeof($resp['group']);

    $data_starttime = null;
    $data_endtime = null;

    foreach($resp as $key => $data_group) {
        echo $key . " => " . $data_group . "<br>";
        foreach($data_group as $key1 => $index_of_group) {
            echo $key . " => " . $key1 . " => " . $index_of_group . "<br>";
            foreach($index_of_group as $key2 => $value) {
                echo $key . " => " . $key1 . " => " . $key2 . " => " . $value . "<br>";

                // tratamento para as variáveis
                if ($key2 == 'startTime') {
                    // Hora incial de captura dos dados
                    $data_starttime = $value;
                }
                if ($key2 == 'endTime') {
                    // Hora incial de captura dos dados
                    $data_endtime = $value;
                }
                if ($key2 == 'sampleSet') {
                    echo $key . " => " . $key1 . " => " . $key2 . " => " . $value . "<br>";
                    ProcessSamples($value);
                }
                //echo $key . " => " . $key1 . " => " . $key2 . " => " . $value . "<br>";
            }
        }
    }

    $startDateTime = new DateTime();
    $startDateTime->setTimestamp($data_starttime/1000);
    echo "Hora inicial captura: " . $startDateTime->format('d/m/Y H:i:s');
    echo "\r\n<br>";

    $endDateTime = new DateTime();
    $endDateTime->setTimestamp($data_endtime/1000);
    echo "Hora inicial captura: " . $endDateTime->format('d/m/Y H:i:s');
    echo "\r\n<br>";


//    //    echo "## Número de amostras: ";
//    echo sizeof($resp['group'][0]['sampleSet']);
//    echo "\r\n<br>";
//
//    echo "\r\n<br>";

    /// mapValue está vazio!!!!!
//
//    foreach($resp['group'][0]['sampleSet'][0] as $key => $value) {
//        foreach($value as $key1 => $value1) {
//            foreach($value1 as $key2 => $value2) {
//                echo $key . " => " . $key1 . " => " . $key2 . " => " . $value2 . "<br>";
//
//                if ($key2 == 'startTime') $startime = $value2;
//                if ($key2 == 'endTime') $endtime = $value2;
//                if ($key2 == 'dataTypeName') {
//                    // verifica qual tipo de informação
//                    // passo?
//                    if ($value2 == 'com.huawei.continuous.steps.delta') {
//                        $values = $value2;
//                    }
//                }
//
//
//
//            }
//        }
//    }
//
//    echo "\r\n<br>";
//
//    foreach($resp['group'][0]['sampleSet'][0]['samplePoints'][0] as $key => $value) {
//        foreach($value as $key1 => $value1) {
//            foreach($value1 as $key2 => $value2) {
//                echo $key . " => " . $key1 . " => " . $key2 . " => " . $value2 . "<br>";
//            }
//        }
//    }


}



?>
<?php
/**

 */

/** O nome do banco de dados*/
define('DB_NAME', 'solution_database');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'solution_root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'qyx366S5%');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');


mysqli_report(MYSQLI_REPORT_STRICT);

function open_database() {
    try {
        $conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        //$conn->set_charset("utf8");
        return $conn;
    } catch (Exception $e) {
        echo $e->getMessage();
        return null;
    }
}

function close_database($conn) {
    try {
        mysqli_close($conn);
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}

/**
 * Salva o contato
 */
function SaveContact($str_name, $str_email) {

    $database = open_database();

    $sql = "INSERT INTO pc_new_user_permission (pc_new_user_permission_name, pc_new_user_permission_email, 
      pc_new_user_permission_add_timestamp) VALUES  ('" . $str_name . "','" . $str_email . "', now());";

    try {

        //echo "<br><br><br>" . $sql;
        //exit;

        if (!$database->query($sql)) {
            echo '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Erro!</strong> Registro não gravado.</div>';
            //sleep(3);
        } else {
            $_SESSION['message'] = 'Registro cadastrado com sucesso.';
            $_SESSION['type'] = 'success';
//            echo '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert">&times;</a>
//            <strong>Sucesso!</strong> Registro gravado com sucesso.</div>';
            //sleep(3);
        }

    } catch (Exception $e) {

        $_SESSION['message'] = 'Nao foi possivel realizar a operacao.';
        $_SESSION['type'] = 'danger';

        echo '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Erro!</strong> Registro não gravado.</div>';
    }

    close_database($database);
}

function create_guid() { // Create GUID (Globally Unique Identifier)
    $guid = '';
    $namespace = rand(11111, 99999);
    $uid = uniqid('', true);
    $data = $namespace;
    $data .= $_SERVER['REQUEST_TIME'];
    $data .= $_SERVER['HTTP_USER_AGENT'];
    $data .= $_SERVER['REMOTE_ADDR'];
    $data .= $_SERVER['REMOTE_PORT'];
    $hash = strtoupper(hash('ripemd128', $uid . $guid . md5($data)));
    $guid = substr($hash,  0,  8) . '-' .
        substr($hash,  8,  4) . '-' .
        substr($hash, 12,  4) . '-' .
        substr($hash, 16,  4) . '-' .
        substr($hash, 20, 12);
    return $guid;
}

function SaveContactAuthorization($accessToken, $refreshToken, $user) {

    $database = open_database();

    // verifica se já existe o cadastro do email
    try {

        $sql = "SELECT * FROM pc_user_permission WHERE pc_user_permission_platform_email = '" . $user->email . "'";

        $res = $database->query($sql);

        //print_r($res);
        //echo "<br>" . $res->num_rows;


        // Se o usuário existe, faz update
        if ($res->num_rows > 0) {

            $sql = "UPDATE pc_user_permission
                    SET
                    pc_user_permission_access_token = '" . $accessToken .  "',
                    pc_user_permission_refresh_token ='" . $refreshToken . "',
                    pc_user_permission_enabled = 1,
                    pc_user_permission_authorization_timestamp = now(),
                    pc_user_permission_enabled_timestamp = now(),
                    pc_user_permission_last_timestamp = NULL
                    WHERE pc_user_permission_platform_email = '" . $user->email . "'";

        } else {

            // Gera o GUID para o usuário
            $GUID = create_guid();

            $sql = "INSERT INTO pc_user_permission (pc_user_permission_platform_name, pc_user_permission_platform_email,
              pc_user_permission_add_timestamp, pc_user_permission_access_token, pc_user_permission_refresh_token, 
              pc_user_permission_GUID, pc_user_permission_enabled, pc_user_permission_enabled_timestamp, 
              pc_user_permission_authorization_timestamp) VALUES  ('"
                . $user->name . "','" . $user->email . "', now(), '" . $accessToken . "','" . $refreshToken . "','" .
                $GUID . "', 1, now(), now());";
        }

        //echo "SQL: " . $sql;

        if (!$database->query($sql)) {
            //echo '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert">&times;</a>
            //<strong>Erro!</strong> Registro não gravado. Msg: ' . $database->error . '</div>';
            //sleep(3);
            close_database($database);
            return false;
        } else {
            $_SESSION['message'] = 'Registro cadastrado com sucesso.';
            $_SESSION['type'] = 'success';
            //            echo '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert">&times;</a>
            //            <strong>Sucesso!</strong> Registro gravado com sucesso.</div>';
            //sleep(3);
            close_database($database);
            return true;
        }

    } catch (Exception $e) {

        $_SESSION['message'] = 'Nao foi possivel realizar a operacao.';
        $_SESSION['type'] = 'danger';
        close_database($database);
        return false;
        //echo '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert">&times;</a>
        //    <strong>Erro!</strong> Registro não gravado. Msg: ' . $e->getMessage() . '</div>';
    }

}


/**
 *  Insere um registro no BD
 */
function save($table = null, $data = null, $show_success = null) {

    $database = DB::getInstance();
    //$database = open_database();

    $columns = null;
    $values = null;

    //print_r($data);

    foreach ($data as $key => $value) {
        $valueup = strtoupper($value);
        $columns .= trim($key, "'") . ",";
        $values .= "\"$valueup\",";
    }

    // remove a ultima virgula
    $columns = rtrim($columns, ',');
    $values = rtrim($values, ',');

    $sql = "INSERT INTO " . $table . "($columns)" . " VALUES " . "($values);";

    try {

        //echo "<br><br><br>" . $sql;
        //exit;

        if (!$database->query($sql)) {
            echo '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Erro!</strong> Registro não gravado.</div>';
            //sleep(3);
        } else {

            $_SESSION['message'] = 'Registro cadastrado com sucesso.';
            $_SESSION['type'] = 'success';

            if ($show_success == false) {
            } else {
                echo '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Sucesso!</strong> Registro gravado com sucesso.</div>';
            }
            //sleep(3);
        }

    } catch (Exception $e) {

        $_SESSION['message'] = 'Nao foi possivel realizar a operacao.';
        $_SESSION['type'] = 'danger';

        echo '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Erro!</strong> Registro não gravado.</div>';
    }

    //close_database($database);
    //exit;
}


/**
 *  Atualiza um registro em uma tabela, por ID
 */
function update($table = null, $id = 0, $data = null) {

    $database = open_database();

    $items = null;

    foreach ($data as $key => $value) {
        $items .= trim($key, "'") . "='$value',";
    }

    // remove a ultima virgula
    $items = rtrim($items, ',');

    $sql  = "UPDATE " . $table;
    $sql .= " SET $items";
    $sql .= " WHERE id=" . $id . ";";

    echo '<br>' . $sql;
    exit;

    try {
        $database->query($sql);

        $_SESSION['message'] = 'Registro atualizado com sucesso.';
        $_SESSION['type'] = 'success';

    } catch (Exception $e) {

        $_SESSION['message'] = 'Nao foi possivel realizar a operacao.';
        $_SESSION['type'] = 'danger';
    }

    close_database($database);
}

/**
 *  Remove uma linha de uma tabela pelo ID do registro
 */
function remove( $table = null, $id = null ) {

    $database = open_database();

    try {
        if ($id) {

            $sql = "DELETE FROM " . $table . " WHERE id = " . $id;
            $result = $database->query($sql);

            //echo $sql;
            //exit;

            if ($result = $database->query($sql)) {
                $_SESSION['message'] = "Registro Removido com Sucesso.";
                $_SESSION['type'] = 'success';
            }
        }
    } catch (Exception $e) {

        $_SESSION['message'] = $e->GetMessage();
        $_SESSION['type'] = 'danger';
    }

    close_database($database);
}


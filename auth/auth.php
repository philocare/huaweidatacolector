<?php

if (!isset($_GET["code"])) {

    //echo "<h1>Início</h1><br><br>";

    // 1. Generate an authorization code
    // url de requisição do código oAuth
    $oauth_url = "https://oauth-login.cloud.huawei.com/oauth2/v3/authorize?";

    // resposta
    $resp_type = "response_type=code";
    // status
    $stt_auth = "&state=state_parameter_passthrough_value";
    // Código Philo
    $client_code = "&client_id=103879243";
    // Url de redirecionamento
    $redir_url = "&redirect_uri=https://rest.philo.solutions/auth/getdata.php";
    // Escopo da leitura
    //$scope_detalis = "&scope=openid+https://www.huawei.com/healthkit/step.read";
    $scope_detalis = "&scope=openid+email+profile+https://www.huawei.com/healthkit/distance.read+" .
        "https://www.huawei.com/healthkit/speed.read+https://www.huawei.com/healthkit/calories.read+" .
        "https://www.huawei.com/healthkit/oxygensaturation.read+https://www.huawei.com/healthkit/stress.read+" .
        "https://www.huawei.com/healthkit/heightweight.read+https://www.huawei.com/healthkit/step.read+" .
        "https://www.huawei.com/healthkit/sleep.read+https://www.huawei.com/healthkit/heartrate.read+" .
        "https://www.huawei.com/healthkit/activity.read+https://www.huawei.com/healthkit/activityrecord.read+" . 
        "https://www.huawei.com/healthkit/hearthealth.read";

    //+https://www.huawei.com/healthkit/step.read+https://www.huawei.com/healthkit/sleep.read";
    // tipo de acesso
    $access_type = "&access_type=offline";
    // display
    $disp = "&display=page";

    // constroi o pedido de get
    $get_str = $oauth_url . $resp_type . $stt_auth . $client_code . $redir_url . $scope_detalis . $access_type . $disp;

    header("Location: " . $get_str);

// Monta o GET
//$ch = curl_init($get_str);

//$return = curl_exec($ch);

//curl_close($ch);

//return $return;
} else {
    // Atribuição de parâmetros
    //$null = "";
    //$dataPath = "D:/php/Apache2.4.41/Apache24/htdocs/json.txt";
    //$dataCollectorId = "raw:com.huawei.instantaneous.height:com.huawei.demo:huawei:ExampleTablet:1000:DataCollectorExample";

}
?>
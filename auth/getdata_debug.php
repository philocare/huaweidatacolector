<?php

require_once "database/data_access.php";

// O token que permite o acesso ao usuário
// Cada usuário terá de ter seu token armazenado para podermos ter acesso ao seus dados.
// Um link de autorização deve ser enviado ao usuário para que ele autorize a Philocare utilizar seus dados
//
// Colocar as autorizações philocare
$steps_samples = array();
$hr_samples = array();

    //echo "Resposta a requisição de TOKEN acesso: <br>";
    //echo "Tempo token: " . $json_res['expires_in'] . "<br>";
    //echo "Escopo: " . $json_res['scope'] . "<br>";
    //echo "Id token: " . $json_res['id_token'] . '<br>';
    //echo 'Refresh token: ' . $json_res['refresh_token'] . '<br>';
    //echo 'Access token: ' . $json_res['access_token'] . '<br>';

    // requisitando informações de usuário
    $url="https://oauth-login.cloud.huawei.com/oauth2/v3/tokeninfo"; //Request address
    $headers = ["Content-Type: application/x-www-form-urlencoded", "Host:oauth-login.cloud.huawei.com"];

    $ch = curl_init();//Inicializar curl
    $timeout = 10;// Tempo limite (unidade: s)
    curl_setopt ($ch, CURLOPT_HTTPHEADER, $headers); //setting the transferred content in the header.
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);//O resultado é uma string e a saída para a tela
    curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);//Definir tempo limite
    curl_setopt ($ch, CURLOPT_CUSTOMREQUEST, "GET");//Solicitar método de envio
    curl_setopt ($ch, CURLOPT_URL, $url);// Solicitar endereço de url

    $postdata = "id_token=CwEAAAAAaRHKVLhVhqRrnBSZbi2nGK2B4E5c2TVqJgwZ20tEZVa5cYDtEF3JkeZub6H7TyAhlPVO5kEyN7Fs5q1FvqhMi/h6gBWHAmjjT7Q8Hc6LYHhmaMUdu6nFs/CKlUV0V3dlKVK6lfxAbw==";

    curl_setopt($ch, CURLOPT_POSTFIELDS,$postdata);

    $result = curl_exec($ch);//Execute curl e obtenha o valor de retorno
    curl_close($ch);

    $user_profile = json_decode($result);

    print_r($user_profile);



?>
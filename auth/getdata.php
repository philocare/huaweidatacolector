<?php

require_once "database/data_access.php";

// O token que permite o acesso ao usuário
// Cada usuário terá de ter seu token armazenado para podermos ter acesso ao seus dados.
// Um link de autorização deve ser enviado ao usuário para que ele autorize a Philocare utilizar seus dados
//
// Colocar as autorizações philocare
$steps_samples = array();
$hr_samples = array();

if (isset($_GET["code"])) {

    $accessCode = $_GET["code"];
    //echo "Code: " . $accessCode . "<br><br>";

    $refreshToken = "";
    $accessToken = "";

    //echo "Requisitando TOKEN<br>";

    $url = "https://oauth-login.cloud.huawei.com/oauth2/v3/token"; //Request address

    $param = array(
        //set value as "authorization_code"；meaning Use Authorization Code to get Access Token and ID Token.
        "grant_type" => "authorization_code",
        //Appid of the application registered on the developer Alliance.
        "client_id" => "103879243",
        //Public key assigned to the application which created in developer alliance,please refrencing the interface description for using.
        "client_secret" => "fb29d356175d3912d33e4bd3316424f504b247b5b367cf2da570cc9e26c9e5ea",
        //Random number which used for verify the code,please refrencing the interface description for using.
        //"code_verifier" => "123444444dfd4sadfsdwew321454567587658776t896fdfgdscvvbfxdgfdgfdsfasdfsdgd233",
        //Callback url of application configuration.
        "redirect_uri" => "https://rest.philo.solutions/auth/getdata.php",
        "code" => $accessCode
    );

    $ch = curl_init();
    $header[] = "Content-Type: application/x-www-form-urlencoded";
    $content = http_build_query($param, "", "&");

    $header[] = "Content-Length: " . strlen($content);
    curl_setopt($ch, CURLOPT_HEADER, true); //setting output include header.
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header); //setting the transferred content in the header.
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, count($param));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // check the source of the certificate or not.
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE); // check the source of the certificate or not.
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // setting not output all content if faild automatically
    $response = curl_exec($ch);
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $result = substr($response, $header_size);
    curl_close($ch);

    $json_res = json_decode($result, JSON_PRETTY_PRINT);

    //echo "Resposta a requisição de TOKEN acesso: <br>";
    //echo "Tempo token: " . $json_res['expires_in'] . "<br>";
    //echo "Escopo: " . $json_res['scope'] . "<br>";
    //echo "Id token: " . $json_res['id_token'] . '<br>';
    //echo 'Refresh token: ' . $json_res['refresh_token'] . '<br>';
    //echo 'Access token: ' . $json_res['access_token'] . '<br>';

    // requisitando informações de usuário
    $url="https://oauth-login.cloud.huawei.com/oauth2/v3/tokeninfo"; //Request address
    $headers = ["Content-Type: application/x-www-form-urlencoded", "Host:oauth-login.cloud.huawei.com"];

    $ch = curl_init();//Inicializar curl
    $timeout = 10;// Tempo limite (unidade: s)
    curl_setopt ($ch, CURLOPT_HTTPHEADER, $headers); //setting the transferred content in the header.
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);//O resultado é uma string e a saída para a tela
    curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);//Definir tempo limite
    curl_setopt ($ch, CURLOPT_CUSTOMREQUEST, "GET");//Solicitar método de envio
    curl_setopt ($ch, CURLOPT_URL, $url);// Solicitar endereço de url


    curl_setopt($ch, CURLOPT_POST, count($param));

    $postdata = "id_token=" . $json_res['id_token'];

    curl_setopt($ch, CURLOPT_POSTFIELDS,$postdata);

    $result = curl_exec($ch);//Execute curl e obtenha o valor de retorno
    curl_close($ch);

    $user_profile = json_decode($result);

    //echo "<br>";
    //print_r($user_profile);
    //echo "<br>";
    //print_r($user_profile->email);
    //print_r($user_profile->display_name);
    //echo "<br>";

    $accessToken = $json_res['access_token'];
    $refreshToken = $json_res['refresh_token'];

    //print_r($json_res);
    //echo "<br>";
    //print_r($user_profile);
    //echo "<br>";

//            $ch = curl_init();//Inicializar curl
//            $timeout = 10;// Tempo limite (unidade: s)
//            //curl_setopt ($ch, CURLOPT_HTTPHEADER, $headers); //setting the transferred content in the header.
//            curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);//O resultado é uma string e a saída para a tela
//            curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);//Definir tempo limite
//            curl_setopt ($ch, CURLOPT_CUSTOMREQUEST, "GET");//Solicitar método de envio
//            curl_setopt ($ch, CURLOPT_URL, $url);// Solicitar endereço de url
//
//            //$postdata = "access_token=" . $json_res['access_token'];
//            //curl_setopt($ch, CURLOPT_POSTFIELDS,$postdata);
//
//            $result = curl_exec($ch);//Execute curl e obtenha o valor de retorno
//            curl_close($ch);
//
//            $user_link = json_decode($result);
//            print_r("User likn: " . $user_link);
//            echo "<br>";
//
//            return;

    //echo "AT: " . $accessToken . "<br>";
    //if (($user_profile->email != "") && ($accessToken != "")) {
    if (($accessToken != "") && ($user_profile->email != "")) {

        if ($user_profile->name == "") $user_profile->name = $user_profile->email;
        // Armazenar em banco de dados o token de refresh e outros dados
        $res = SaveContactAuthorization($accessToken, $refreshToken, $user_profile);
        //########################################################################
        // Falta verificar se a parmissão teve sucesso
        if ($res) {
            //header("Location: /thanks.html");
        } else {
            //header("Location: /already.html");
        }


        // requisitando o link para Health kit
        $url="https://h5hosting.dbankcdn.com/cch5/healthkit/privacy/link.html?access_token=" . urlencode($json_res['access_token']);

        //echo "Link";
        header("Location: " . $url);

        //echo "Autorizado!";
    } else {
        header("Location: /error.html");
    }

}



?>
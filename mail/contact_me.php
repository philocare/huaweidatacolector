<?php



use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once "../auth/database/data_access.php";

require 'PHPMailer/Exception.php';
require 'PHPMailer/PHPMailer.php';
require 'PHPMailer/SMTP.php';

/*$string = file_get_contents("config.json");
$option = json_decode($string);

define("MAIL_HOST", $option->MAIL_HOST);
define("MAIL_TITLE", $option->MAIL_TITLE);*/

$str_to_save = "";

try {

    if( isset($_GET['name']) && isset($_GET['email'])) {
        $str_name = $_GET['name'];
        $str_email = $_GET['email'];
        //$str_msg = nl2br($_GET['contact_message']);

        // Salva o cadastro no BD
        SaveContact($str_name, $str_email);

        $str_msg = 'Nome: ' . $str_name . ' Email: ' . $str_email;

        //echo "Send mail";

        // Instancio a classe PHPMailer
        $msg = new PHPMailer();

        // Faço todas as configurações de SMTP para o envio da mensagem
        $msg->CharSet = "UTF-8";
        $msg->isSMTP();
        $msg->Host = 'philo.solutions'; //|*SMTP.SEUDOMINIO.COM.BR*|';
        $msg->SMTPAuth = true;
        $msg->Username = 'auth@philo.solutions'; //|*SEU-EMAIL@SEUDOMINIO.COM.BR*|';
        $msg->Password = '?vqC2HME;-&M';
        $msg->Port = 587;
        $msg->SMTPAutoTLS = false;
        $msg->AuthType = 'PLAIN';

        //Defino o remetente da mensagem
        $msg->setFrom('auth@philo.solutions', 'Auth Philocare');

        // Defino a quem esta mensagem será respondida, no caso, para o e-mail
        // que foi cadastrado no formulário
        $msg->addReplyTo($str_email, $str_name);

        // Defino a mensagem como mensagem de texto (Ou seja não terá formatação HTML)
        $msg->IsHTML(false);

        // Adiciono o destinatário desta mensagem, no caso,
        //minha conta de contatos comerciais.
        //$msg->AddAddress('care@philo.care', 'Philocare');
        //$msg->AddAddress('robson@philo.care', 'Robson');
        //$msg->AddAddress('rocha@philo.care', 'Rocha');
        //$msg->AddAddress('douglas@philo.care', 'Douglas');
        $msg->AddAddress('robson.pierangeli@gmail.com', 'Robson');
        $msg->AddAddress('auth@philo.solutions', 'Autenticações');

        // Defino o assunto que foi digitado no formulário
        $msg->Subject = 'Autorização recebida';

        // Defino a mensagem que foi digitada no formulário
        $msg->Body = $str_msg;

        // Defino a mensagem alternativa que foi digitada no formulário.
        // Esta mensagem é utilizada para validações AntiSPAM e por isto
        // é muito recomendado que utilize-a
        $msg->AltBody = $str_msg;

        // Faço o envio da mensagem
        $enviado = $msg->Send();

        // Limpo todos os registros de destinatários e arquivos
        $msg->ClearAllRecipients();

        // Caso a mensagem seja enviada com sucesso ela retornará sucesso
        // senão, ela retornará o erro ocorrido
        if ($enviado) {
            //echo "Autorização enviada com sucesso!";
            //header('Location: /index.html');
            //die();
        } else {
            //echo "Não foi possível enviar a Autorização.";
            //echo "<b>Informações do erro:</b> " . $msg->ErrorInfo;
        }

        $str_to_save = "Nome: " . $str_name . "#Mail: " . $str_email . "\r\n";

        // salva em arquivo os contatos
        file_put_contents("autorizations.txt", $str_to_save, FILE_APPEND);
    }
} catch (Exception $e) {
    // Aconteceu erro
    file_put_contents("autorizations.txt", $str_to_save, FILE_APPEND);
} finally {
    header("Location: https://rest.philo.solutions/auth/auth.php");
}

?>

